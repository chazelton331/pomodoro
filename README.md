# Pomodoro

### Description

A timer for pair programming.

### How to run it

```
./pomodoro
```

### Collaborators

[@jpotts244](https://github.com/jpotts244)
